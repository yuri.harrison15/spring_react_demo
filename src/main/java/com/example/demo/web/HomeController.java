package com.example.demo.web;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class HomeController {

    @GetMapping("")
    public String index(Model model){
        model.addAttribute("name", "Index");
        return "index";
    }

    @GetMapping("a")
    public String indexA(Model model){
        model.addAttribute("name", "A");
        return "index";
    }

    @GetMapping("b")
    public String indexB(@RequestParam(name="name", required=false, defaultValue="B") String name, Model model){
        model.addAttribute("name", name);
        return "index";
    }
}