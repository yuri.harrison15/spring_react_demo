package com.example.demo.web;

import org.springframework.web.bind.annotation.RestController;

import java.util.concurrent.TimeUnit;

import com.example.demo.domain.Person;

import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestMapping;

@RestController
@RequestMapping("/api")
public class APIController {

    @RequestMapping("")
    public String index() {
        String asdf = MediaType.APPLICATION_JSON.toString();
        System.out.println(asdf);
        return "API Home";
    }

    @RequestMapping("person")
    public Person person() {
        try {
            TimeUnit.SECONDS.sleep(2);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return new Person(1, "João", "This is the content.");
    }
}