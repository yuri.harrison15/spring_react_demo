package com.example.demo.domain;


public class Person {

    private final long id;
    private final String name;
    private final String content;

    public Person(long id, String name, String content) {
        this.id = id;
        this.name = name;
        this.content = content;
    }

    public long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getContent() {
        return content;
    }
}