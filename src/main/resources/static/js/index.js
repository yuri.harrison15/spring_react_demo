'use strict';

const e = React.createElement;

class LikeButton extends React.Component {
  constructor(props) {
    super(props);
    this.state = { liked: false };
  }

  render() {
    // Js Object
    // JSX
    if (this.state.liked) {
      return e('p', 'You liked this.');
    //   return <p>You liked this.</p>;
    }

    // return e(
    //   'button',
    //   { onClick: () => this.setState({ liked: true }) },
    //   'Like'
    // );
    return (
        <button onClick={() => this.setState({ liked: true })}>
        Like
        </button>
    )
  }
}

const ENDPOINT = 'http://localhost:8081/api/person'

class Person extends React.Component{
    constructor(props) {
        super(props)
        this.state = { loading: false, error: false, data: null};
    }

    // componentDidMount(){
        
    // }

    loadData = () => {
        this.setState({loading: true});
        
        fetch(ENDPOINT)
            .then(response => {
                if (response.ok) {

                    return response.json();
                }
                else {
                    throw new Error("Couldn't fetch the data.");
                }
            })
            .then(data => this.setState({ data, loading: false }))
            .catch(error => this.setState({ error, loading: false }))
    }

    render(){
        const { loading, error, data } = this.state;

        if (error){
            return (
                <div class="alert alert-danger" role="alert">
                    { error.message }
                </div>
            )
        } else if (loading) {
            return (
                <button class="btn btn-primary" type="button" disabled>
                    <span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>
                    <span>Loading...</span>
                </button>
            )
        } else if (!data){
            return (
                <button class="btn btn-primary" type="button" onClick={this.loadData}>
                    LOAD DATA
                </button>
            )
        } else {
            return (
                <div class="card" style={{width: '18rem'}}>
                    <div class="card-body">
                        <h5 class="card-title">{data.name}</h5>
                        <h6 class="card-subtitle mb-2 text-muted">ID {data.id}</h6>
                        <p class="card-text">{data.content}</p>
                    </div>
                </div>
            )
        }
    }
}


const domContainer = document.querySelector('#react_content');
ReactDOM.render(<Person />, domContainer);